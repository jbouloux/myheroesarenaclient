export interface Power {
    power_uuid: string;
    power_name: string;
    power_intensity: number;
    power_energy_cost: number;
}

export interface HeroCard {
    hero_uuid: string;
    hero_lvl: number;
    hero_name: string;
    hero_realname: string;
    hero_health: number;
    hero_strength: number;
    hero_energy: number;
    hero_avatar: string;
    hero_powers: Array<Power>;
    hero_price: number;
    collapsed: boolean;
}