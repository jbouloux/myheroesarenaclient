import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { HeroCard } from './hero.class';
import { UserService } from '../../services/user.service';
import { ShopService } from '../../services/shop.service';
import { takeWhile } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-hero-card',
  templateUrl: './hero-card.component.html',
  styleUrls: ['./hero-card.component.scss']
})
export class HeroCardComponent implements OnInit, OnDestroy {

  @Input() hero: HeroCard = {} as HeroCard;
  private alive = true;

  constructor(public sanitizer: DomSanitizer,
      public userService: UserService,
      private shopService: ShopService,
      private router: Router) { }
  ngOnDestroy(): void {
    this.alive = false;
  }

  ngOnInit(): void {
  }

  buyHero(hero_uuid) {
    this.shopService.buyHero(hero_uuid).pipe(takeWhile(() => this.alive)).subscribe((result: any) => {
      if (result.success) {
        this.router.navigateByUrl('/heroes');
      } else {
        console.error(result['message'])
      }
    })
  }

}
