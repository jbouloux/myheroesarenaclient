import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { HomeComponent } from '../home/home.component';
import { GlobalModule } from '../global.module';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from '../interceptors/auth.interceptor';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { MyHeroesComponent } from '../my-heroes/my-heroes.component';
import { ShopComponent } from '../shop/shop.component';
import { ProfileComponent } from './navbar/profile/profile.component';
import { HeroCardComponent } from './hero-card/hero-card.component';
import { HeroCarouselComponent } from './hero-carousel/hero-carousel.component';
import { BattlesComponent } from '../battles/battles.component';
import { CurrentComponent } from '../battles/current/current.component';
import { BattleCardComponent } from '../battles/battle-card/battle-card.component';
import { BattlefieldComponent } from '../battles/battlefield/battlefield.component';
import { HeroChoiceDialogComponent } from '../battles/hero-choice-dialog/hero-choice-dialog.component';

@NgModule({
  declarations: [LayoutComponent, HomeComponent, NavbarComponent, FooterComponent, MyHeroesComponent, ShopComponent, ProfileComponent, HeroCardComponent, HeroCarouselComponent, BattlesComponent, CurrentComponent, BattleCardComponent, BattlefieldComponent, HeroChoiceDialogComponent],
  imports: [
    CommonModule,
    LayoutRoutingModule,
    GlobalModule
  ],
  entryComponents: [ProfileComponent],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ]
})
export class LayoutModule { }
