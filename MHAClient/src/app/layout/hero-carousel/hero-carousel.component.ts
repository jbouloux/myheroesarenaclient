import { Component, OnInit, Input } from '@angular/core';
import { HeroCard } from '../hero-card/hero.class';

@Component({
  selector: 'app-hero-carousel',
  templateUrl: './hero-carousel.component.html',
  styleUrls: ['./hero-carousel.component.scss']
})
export class HeroCarouselComponent implements OnInit {

  @Input() heroes: Array<HeroCard> = [];

  constructor() { }

  ngOnInit(): void {
  }

}
