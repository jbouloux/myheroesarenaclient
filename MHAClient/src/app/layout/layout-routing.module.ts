import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';
import { HomeComponent } from '../home/home.component';
import { MyHeroesComponent } from '../my-heroes/my-heroes.component';
import { ShopComponent } from '../shop/shop.component';
import { BattlesComponent } from '../battles/battles.component';
import { UserGuard } from '../guards/user.guard';
import { BattlefieldComponent } from '../battles/battlefield/battlefield.component';

const routes: Routes = [
  {path: '', redirectTo: 'home'},
  {path:'', children: [
    {path: '', canActivate: [UserGuard], component: LayoutComponent, children: [
      {path: 'home', component: HomeComponent},
      {path: 'heroes', component: MyHeroesComponent},
      {path: 'shop', component: ShopComponent},
      {path: 'battles', component: BattlesComponent},
      {path: 'battlefield/:battle_uuid', component: BattlefieldComponent}
    ]}
  ]},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule { }
