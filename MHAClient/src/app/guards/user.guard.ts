import { Injectable, OnDestroy } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { takeWhile } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserGuard implements CanActivate, OnDestroy {

  private alive = true;

  constructor(private authService: AuthService, private router: Router) {}
  ngOnDestroy(): void {
    this.alive = false;
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      let token = localStorage.getItem('token');
      let subject = new Subject<boolean>();
      
      if (!token){
        return false;
      } else {
        this.authService.checkUserToken().pipe(takeWhile(() => this.alive)).subscribe(ret => {
          if (ret === true)
            subject.next(true);
        }, (error) => {
          localStorage.removeItem('token');
          localStorage.removeItem('user');
          this.router.navigate(['']);
          subject.next(false);
        })
      }

      return subject.asObservable();
  }
  
}
