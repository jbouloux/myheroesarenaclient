import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { takeWhile, timeout } from 'rxjs/operators';
import { BattleService } from '../../services/battle.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-battlefield',
  templateUrl: './battlefield.component.html',
  styleUrls: ['./battlefield.component.scss']
})
export class BattlefieldComponent implements OnInit, OnDestroy {

  battle: any;
  battle_uuid;
  alive: any = true;

  ia_hero_dead = false;
  user_1_hero_dead = false;
  message = '';
  your_turn = false;

  constructor(public sanitizer: DomSanitizer, private route: ActivatedRoute, private battleService: BattleService, private router: Router) {
    this.route.params.pipe(takeWhile(() => this.alive)).subscribe(params => {
      this.battle_uuid = params.battle_uuid;
      this.battleService.getCurrentBattleDetails(this.battle_uuid).pipe(takeWhile(() => this.alive)).subscribe(battle => {
        this.battle = battle;
        console.log(this.battle)
        this.getWhosTurn()
      })
    })

  }

  getWhosTurn() {
    if (this.battle.next_attack === this.battle.user_1.user_uuid) {
      this.displayMessage("C'est au tour de votre héros ! A l'attaque !")
      setTimeout(() => {
        this.your_turn = true;
      }, 5000);
    } else {
      this.your_turn = false;
      this.displayMessage(`Au tour de ${this.battle.ia.hero_name} ennemi. ${this.battle.ia.user_username} réflechi à la prochaine attaque...`);
      setTimeout(() => {
        this.ia_attack();
      }, 6000);
    }
  }

  attack(from_uuid, target_uuid, power) {
    let from = this.battle.user_1.user_uuid === from_uuid ? this.battle.user_1.hero_name : this.battle.ia.hero_name + ' ennemi';
    let target = this.battle.user_1.user_uuid === target_uuid ? this.battle.user_1.hero_name : this.battle.ia.hero_name + ' ennemi';

    this.displayMessage(from + ' attaque ' + target + ' avec la technique ' + power.power_name + ' !');
    setTimeout(() => {
      this.battleService.battleAttack(this.battle_uuid,
        from_uuid, target_uuid, power.power_uuid).pipe(takeWhile(() => this.alive)).subscribe((data: any) => {
          console.log(data);
          if (!data)
            location.reload();
          else {
            this.battle = data.battleData;
            if (data.winner) {
              if (this.battle.user_1.user_uuid === data.winner) {
                this.displayMessage(power.power_name + ' inflige ' + data.damages + ' de dégâts à ' + target + ' !');
                setTimeout(() => {
                  this.displayMessage(target + ' est KO. Vous remportez la victoire ! Bravo !');
                  setTimeout(() => {
                    this.displayMessage('Votre ' + from + ' gagne ' + data.hero_lvl_up + ' niveau. Il passe donc au niveau ' +
                      (this.battle.user_1.hero_lvl + data.hero_lvl_up));
                    setTimeout(() => {
                      this.displayMessage('Vous gagnez également ' + data.credits + ' crédits !');
                      setTimeout(() => {
                        this.router.navigateByUrl('/battles');
                      }, 5000);
                    }, 5000);
                  }, 5000);
                }, 6000);
              } else {
                this.displayMessage(this.battle.user_1.hero_name + ' est KO. ' + this.battle.ia.user_username + ' remporte la victoire.' );
                setTimeout(() => {
                  this.router.navigateByUrl('/battles');
                }, 5000);
              }
            } else {
              this.displayMessage(power.power_name + ' inflige ' + data.damages + ' de dégâts à ' + target + ' !');
              setTimeout(() => {
                this.getWhosTurn();
              }, 6000);
            }
          }
        })
    }, 6000);
  }

  getRandomNumber(min, max) {
    return Math.round(Math.random() * (max - min) + min);
  }

  ia_attack() {
    console.log(this.battle.ia.powers)
    let power = {power_uuid: null};
    while (power.power_uuid === null) {
      power = this.battle.ia.powers[this.getRandomNumber(0, this.battle.ia.powers.length - 1)];
    }
    this.attack(this.battle.ia.user_uuid, this.battle.user_1.user_uuid, power);
  }

  user_attack(power) {
    this.your_turn = false;
    this.attack(this.battle.user_1.user_uuid, this.battle.ia.user_uuid, power);
  }


  ngOnDestroy(): void {
    this.alive = false;
  }

  ngOnInit(): void {
  }

  displayMessage(message) {
    console.log(message);
    this.message = '';
    let time = 60;
    for (let i = 0; i < message.length; i++) {
      setTimeout(() => {
        this.message += message[i];
      }, time);
      time += 60;
    }
  }

}
