import { Component, OnInit, OnDestroy } from '@angular/core';
import { BattleService } from '../services/battle.service';
import { takeWhile } from 'rxjs/operators';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router'


@Component({
  selector: 'app-battles',
  templateUrl: './battles.component.html',
  styleUrls: ['./battles.component.scss']
})
export class BattlesComponent implements OnInit, OnDestroy {

  private alive = true;
  battles = [];
  public heroes = [];

  constructor(private battleService: BattleService, private userService: UserService, private router: Router) {
    this.battleService.getCurrentBattles().pipe(takeWhile(() => this.alive)).subscribe((battles: Array<any>) => {
      console.log(battles)
      if (battles && battles.length) {
        this.battles = [...battles];

      }
    })

    this.userService.getUserHeroes().pipe(takeWhile(() => this.alive)).subscribe((data: any) => {
      this.heroes = data;
    })

  }

  ngOnInit(): void {
  }

  getBattlesDetails() {
    this.battles = [...this.battles.map(async battle => {
      battle['details'] = await this.battleService.getCurrentBattleDetails(battle.battle_uuid).toPromise().then(data => {return data})
      return battle;
    })]
  }

  createBattle(hero) {
    this.battleService.createBattle(hero.hero_uuid).pipe(takeWhile(() => this.alive)).subscribe((ret: any) => {
      if (ret && ret.battle_uuid) {
        this.router.navigateByUrl('/battlefield/' + ret.battle_uuid);
      }
    })
  }

  ngOnDestroy() {
    this.alive = false;
  }

}
