import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-hero-choice-dialog',
  templateUrl: './hero-choice-dialog.component.html',
  styleUrls: ['./hero-choice-dialog.component.scss']
})
export class HeroChoiceDialogComponent implements OnInit {

  @Input() heroes = [];

  constructor() { }

  ngOnInit(): void {
  }

}
