import { Component, OnInit, Input } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';


@Component({
  selector: 'app-battle-card',
  templateUrl: './battle-card.component.html',
  styleUrls: ['./battle-card.component.scss']
})
export class BattleCardComponent implements OnInit {

  @Input() battle: any;
  resume = false;

  constructor(public sanitizer: DomSanitizer, private router: Router) { }

  ngOnInit(): void {
    console.log(this.battle)
  }

  cancelResume() {
    setTimeout(() => {
      this.resume = false;
    }, 50);
  }

  resumeConfirm() {

  }

  resumeBattle() {
    console.log('Resuming Battle');
    this.router.navigateByUrl('/battlefield/' + this.battle.battle_uuid);
  }

}
