import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-current',
  templateUrl: './current.component.html',
  styleUrls: ['./current.component.scss']
})
export class CurrentComponent implements OnInit {

  @Input() battles = [];

  constructor() { }

  ngOnInit(): void {
    
  }

}
