import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ShopService {
  private API_URL = environment.api_url;

  constructor(private http: HttpClient) { }

  getHeroes() {
    return this.http.get(this.API_URL + '/user/shop');
  }

  buyHero(hero_uuid) {
    return this.http.get(this.API_URL + '/user/buyhero/' + hero_uuid);
  }
}
