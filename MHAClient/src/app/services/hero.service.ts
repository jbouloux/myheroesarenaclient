import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class HeroService {

  private API_URL = environment.api_url;

  constructor(private http: HttpClient) {
    
  }

  getPower(power_uuid) {
    return this.http.get(this.API_URL + '/power/' + power_uuid);
  }
}
