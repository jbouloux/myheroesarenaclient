import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BattleService {

  private API_URL = environment.api_url;

  constructor(private http: HttpClient) { }

  createBattle(hero_uuid) {
    return this.http.post(this.API_URL + '/battle', {hero_uuid: hero_uuid});
  }

  getCurrentBattles() {
    return this.http.get(this.API_URL + '/battle/currents');
  }

  getCurrentBattleDetails(battle_uuid) {
    return this.http.get(this.API_URL + '/battle/current/' + battle_uuid);
  }

  battleAttack(battle_uuid, from_uuid, target_uuid, power_uuid) {
    return this.http.post(this.API_URL + '/battle/attack', {
      battle_uuid: battle_uuid,
      from_uuid: from_uuid,
      target_uuid: target_uuid,
      power_uuid: power_uuid})
  }
}
