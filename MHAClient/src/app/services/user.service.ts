import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private API_URL = environment.api_url;

  userCredits = 0;

  constructor(private http: HttpClient) { }

  getUserCredits() {
    return this.http.get(this.API_URL + '/user/credits');
  }

  getUserHeroes() {
    return this.http.get(this.API_URL + '/user/heroes');
  }

}
