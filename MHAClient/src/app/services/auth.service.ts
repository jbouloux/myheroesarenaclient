import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private API_URL = environment.api_url;

  constructor(private http: HttpClient) { }

  checkIfUsernameExists(username) {
    return this.http.get(this.API_URL + '/auth/username_exist/' + username);
  }

  checkIfEmailExists(email) {
    return this.http.get(this.API_URL + '/auth/email_exist/' + email);
  }

  register(user) {
    delete user.user_passwordconf;
    return this.http.post(this.API_URL + '/auth/signup', user);
  }

  login(user) {
    return this.http.post(this.API_URL + '/auth/signin', user);
  }

  forgotPwd(email) {
    return this.http.post(this.API_URL + '/auth/forgot_password/', {user_email: email});
  }

  changeForgottenPassword(new_pwd, activation_uuid) {
    return this.http.patch(this.API_URL + '/auth/forgot_password', { activation_uuid: activation_uuid, user_password: new_pwd});
  }

  checkForgotPasswordUuid(activation_uuid) {
    return this.http.get(this.API_URL + '/auth/forgot_password/check/' + activation_uuid);
  }

  activateAccount(activation_uuid) {
    return this.http.get(this.API_URL + '/auth/activation/' + activation_uuid);
  }

  checkUserToken() {
    return this.http.get(this.API_URL + '/user/isConnected');
  }

  checkAdminToken() {
    return this.http.get(this.API_URL + '/admin/isConnected');
  }

}
