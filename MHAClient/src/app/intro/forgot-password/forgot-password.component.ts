import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { takeWhile } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit, OnDestroy {

  user_password = '';
  user_passwordconf = '';
  activation_uuid
  errors = [];
  alive: boolean = true;
  message = '';

  constructor(private authService: AuthService, private activatedRoute: ActivatedRoute, private router: Router) {
    this.activatedRoute.params.pipe(takeWhile(() => this.alive)).subscribe(params => {
      this.activation_uuid = params.uuid;
      console.log(this.activation_uuid);
      this.authService.checkForgotPasswordUuid(this.activation_uuid).pipe(takeWhile(() => this.alive)).subscribe(ret => {
        if (!ret) {
          this.message = 'L\'uuid renseigné n\'est pas valide.';
          setTimeout(() => {
            router.navigateByUrl('/');
          }, 2000);
        }
      })
    })
  }

  ngOnDestroy(): void {
    this.alive = false;
  }

  ngOnInit(): void {
  }

  send() {
    this.errors = [];
    if (!this.user_password) {
      this.errors.push('Veuillez renseigner un nouveau mot de passe')
    } else if (!this.user_password.match(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/)) {
      this.errors.push('Le mot de passe doit contenir au minimum 8 caractères, 1 lettre et 1 chiffre');
    } else if (!this.user_passwordconf) {
      this.errors.push('Veuillez répéter le nouveau mot de passe');
    } else if (this.user_passwordconf != this.user_password) {
      this.errors.push('Les mots de passes ne sont pas identiques');
    } else {
      this.authService.changeForgottenPassword(this.user_password, this.activation_uuid).pipe(takeWhile(() => this.alive)).subscribe(ret => {
        this.message = ret ? 'Votre mot de passe a été modifié avec succès :)' : 'Erreur lors du changement de mot de passe. veuillez réessayer ultérieurement.';
        setTimeout(() => {
          this.router.navigateByUrl('/');
        }, 2000);
      })
    }
  }

}
