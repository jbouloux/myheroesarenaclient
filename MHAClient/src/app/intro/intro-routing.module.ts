import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IntroComponent } from './intro.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ActivationComponent } from './activation/activation.component';


const routes: Routes = [
  {path: '', children: [
    {path: '', component: IntroComponent},
    {path: 'forgot_password/:uuid', component: ForgotPasswordComponent},
    {path: 'activation/:uuid', component: ActivationComponent},
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IntroRoutingModule { }
