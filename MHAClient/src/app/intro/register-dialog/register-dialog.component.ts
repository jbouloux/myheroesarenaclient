import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, AsyncValidatorFn, AbstractControl, ValidationErrors, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { timer, Observable } from 'rxjs';
import { switchMap, map, takeWhile } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-register-dialog',
  templateUrl: './register-dialog.component.html',
  styleUrls: ['./register-dialog.component.scss']
})
export class RegisterDialogComponent implements OnInit, OnDestroy {

  public registerForm: FormGroup;
  public errors = [];
  private alive: boolean = true;
  public message: string = '';

  constructor(private authService: AuthService, private formBuilder: FormBuilder, private snackBar: MatSnackBar) {
    this.registerForm = this.formBuilder.group({
      user_username: ['', {asyncValidators: [this.usernameValidator()], updateOn:'blur'}],
      user_email: ['', { validators: [Validators.email], asyncValidators: [this.emailValidator()], updateOn:'blur'}],
      user_password: ['', Validators.pattern(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/)],
      user_passwordconf: ['']
    }, {validators: [this.checkPasswords] })
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.alive = false;
  }

  
  checkPasswords(group: FormGroup) {
    let pass = group.get('user_password').value;
    let confirmPass = group.get('user_passwordconf').value;

    return pass === confirmPass ? null : { notSame: true }     
  }

  usernameValidator(): AsyncValidatorFn {
    return (control: AbstractControl): Observable<ValidationErrors | null> => {
      return this.authService.checkIfUsernameExists(control.value).pipe(
        map(res => {
          // if res is true, username exists, return true
          return res ? { usernameExists: true } : null;
          // NB: Return null if there is no error
        })
      );
    };
  }

  emailValidator(): AsyncValidatorFn {
    return (control: AbstractControl): Observable<ValidationErrors | null> => {
      return this.authService.checkIfEmailExists(control.value).pipe(
        map(res => {
          // if res is true, email exists, return true
          return res ? { emailExists: true } : null;
          // NB: Return null if there is no error
        })
      );
    };
  }

  getRegisterFormErrors() {
    if (this.registerForm.get('user_username').errors?.required) {
      this.errors.push('Veuillez saisir un nom d\'utilisateur');
    } else if (this.registerForm.get('user_username').errors?.usernameExists)
      this.errors.push('Ce nom d\'utilisateur est déjà utilisé');

    if (this.registerForm.get('user_email').errors?.required) {
      this.errors.push('Veuillez saisir une adresse email');
    } else if (this.registerForm.get('user_email').errors?.email) {
      this.errors.push('Adresse email non valide')
    } else if (this.registerForm.get('user_email').errors?.emailExists) {
      this.errors.push('Cette adresse email est déjà utilisée');
    }

    if (this.registerForm.get('user_password').errors?.required) {
      this.errors.push('Veuillez saisir un mot de passe');
    } else if (this.registerForm.get('user_password').errors?.pattern) {
      this.errors.push('Le mot de passe doit contenir au moins 8 caractères, 1 lettre et 1 chiffre');
    } else if ( this.registerForm.get('user_passwordconf').errors?.required) {
      this.errors.push('Veuillez répéter le mot de passe');
    } else if (this.registerForm.errors?.notSame) {
      this.errors.push('Les mots de passe ne sont pas identiques');
    }
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 5000,
    });
  }

  signUp() {
    this.errors = [];
    setTimeout(() => {
      if (this.registerForm.valid)
        this.authService.register(this.registerForm.value).pipe(takeWhile(() => this.alive)).subscribe(res => {
          if (res)
            this.message = 'Compte créé avec succès ! Veuillez cliquer sur le lien qui vous a été envoyé par email afin de valider votre compte.';
          else
            this.message = 'La création de votre compte a échoué :( Merci de réessayer ultérieurement';
        })
      else {
        this.getRegisterFormErrors();
        console.log('NOT VALID', this.registerForm);
      }
    }, 1000);
  }

}
