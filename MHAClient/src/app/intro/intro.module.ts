import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IntroRoutingModule } from './intro-routing.module';
import { IntroComponent } from './intro.component';
import { GlobalModule } from '../global.module';

import { AuthService } from '../services/auth.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { RegisterDialogComponent } from './register-dialog/register-dialog.component';
import { LoginDialogComponent } from './login-dialog/login-dialog.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ActivationComponent } from './activation/activation.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from '../interceptors/auth.interceptor';

@NgModule({
  declarations: [IntroComponent, RegisterDialogComponent, LoginDialogComponent, ForgotPasswordComponent, ActivationComponent],
  entryComponents: [RegisterDialogComponent, LoginDialogComponent],
  imports: [
    CommonModule,
    IntroRoutingModule,
    GlobalModule
  ], providers: [
    AuthService,
    MatSnackBar,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ]
})
export class IntroModule { }
