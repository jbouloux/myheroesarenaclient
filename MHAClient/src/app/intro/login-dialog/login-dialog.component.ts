import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, SelectControlValueAccessor } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { takeWhile } from 'rxjs/operators';

@Component({
  selector: 'app-login-dialog',
  templateUrl: './login-dialog.component.html',
  styleUrls: ['./login-dialog.component.scss']
})
export class LoginDialogComponent implements OnInit {

  public loginForm: FormGroup;
  public errors = [];
  private alive: boolean = true;
  public message: string = '';
  public forgotPwd = false;
  public emailForgotPwd = '';
  public loading = false;

  constructor(private formBuilder: FormBuilder, private authService: AuthService) {
    this.loginForm = this.formBuilder.group({
      user_username: '',
      user_password: ''
    })
  }

  ngOnInit(): void {
  }

  getLoginErrors() {
    if (this.loginForm.get('user_username').errors?.required)
      this.errors.push('Veuillez renseigner votre nom d\'utilisateur');
    if (this.loginForm.get('user_password').errors?.required)
      this.errors.push('Veuillez renseigner votre mot de passe');
  }

  login() {
    this.loading = true;
    this.errors = [];
    if (!this.loginForm.valid) {
      this.getLoginErrors();
      this.loading = false;
    } else {
      this.authService.login(this.loginForm.value).pipe(takeWhile(() => this.alive)).subscribe((res: any) => {
        this.loading = false;
        if (res.success && res.token) {
          localStorage.setItem('user', JSON.stringify(res.user));
          localStorage.setItem('token', res.token);
          this.message = 'Connexion réussie !';
          setTimeout(() => {
            location.reload();
          }, 2000);
        } else {
          if (res.error = 'wrong_password')
            this.errors.push('Mauvais mot de passe');
          else
            this.errors.push('Oups ! Erreur serveur. Merci de réessayer ultérieurement.')
        }
      })
    }
  }

  getForgotpwd() {
    this.loading = true;
    this.authService.forgotPwd(this.emailForgotPwd).pipe(takeWhile(() => this.alive)).subscribe(ret => {
      if (ret) {
        this.message = "Un email vous a été envoyé afin de redéfinir votre mot de passe."
        setTimeout(() => {
          location.reload();
        }, 2000);
      } else {
        this.message = "L'email renseigné n'existe pas dans notre base.";
        setTimeout(() => {
          location.reload();
        }, 2000);
      }
    })
  }

}
