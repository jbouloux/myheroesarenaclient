import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { takeWhile } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-activation',
  templateUrl: './activation.component.html',
  styleUrls: ['./activation.component.scss']
})
export class ActivationComponent implements OnInit, OnDestroy {

  private alive = true;
  public message = '';

  constructor(private authService: AuthService, private activatedRoute: ActivatedRoute, private router: Router) {
    this.activatedRoute.params.pipe(takeWhile(() => this.alive)).subscribe(params => {
      this.authService.activateAccount(params.uuid).pipe(takeWhile(() => this.alive)).subscribe(ret => {
        if (!ret) {
          this.message = 'L\'uuid renseigné n\'est pas valide.';
          setTimeout(() => {
            router.navigateByUrl('/');
          }, 2000);
        } else {
          this.message = 'Votre compte a été activé avec succès ! Vous pouvez vous connecter :)';
          setTimeout(() => {
            router.navigateByUrl('/');
          }, 2000);
        }
      })
    })
  }

  ngOnDestroy(): void {
    this.alive = false;
  }

  ngOnInit(): void {
  }

}
