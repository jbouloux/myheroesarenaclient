import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { RegisterDialogComponent } from './register-dialog/register-dialog.component';
import { LoginDialogComponent } from './login-dialog/login-dialog.component';

@Component({
  selector: 'app-intro',
  templateUrl: './intro.component.html',
  styleUrls: ['./intro.component.scss']
})

export class IntroComponent implements OnInit{

  public collapsed = true;
  public forcedExpanded = false;
 
  constructor(public dialog: MatDialog) {
  }

  ngOnInit(): void {
  }

  openRegisterModal() {
      const dialogRef = this.dialog.open(RegisterDialogComponent, {minWidth: 400});
  
      dialogRef.afterClosed().subscribe(result => {
        this.forcedExpanded = false;
      });
  }

  openLoginModal() {
    const dialogRef = this.dialog.open(LoginDialogComponent, {minWidth: 400});

    dialogRef.afterClosed().subscribe(result => {
      this.forcedExpanded = false;
    });
}

}