import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { takeWhile } from 'rxjs/operators';
import { HeroService } from '../services/hero.service';
import { HeroCard, Power } from '../layout/hero-card/hero.class';

@Component({
  selector: 'app-my-heroes',
  templateUrl: './my-heroes.component.html',
  styleUrls: ['./my-heroes.component.scss']
})
export class MyHeroesComponent implements OnInit {

  private alive = true;
  public heroes = [];

  constructor(private userService: UserService, private heroService: HeroService) { }

  ngOnInit() {
    this.userService.getUserHeroes().pipe(takeWhile(() => this.alive)).subscribe((heroes: Array<HeroCard>) => {
      if (heroes && heroes.length) {
        this.heroes = [...heroes];
        heroes.map(async h => {
          h['collapsed'] = true;
          h['hero_powers'] = await this.getHeroPowersDetails(h);
          return h
        })
      }
        console.log(this.heroes);
    })
  }

  async getHeroPowersDetails(hero) {
    let powers = [];

    if (hero.hero_power_1)
      powers.push((await this.heroService.getPower(hero.hero_power_1).toPromise())[0] || null);
    if (hero.hero_power_2)
      powers.push((await this.heroService.getPower(hero.hero_power_2).toPromise())[0] || null);
    if (hero.hero_power_3)
      powers.push((await this.heroService.getPower(hero.hero_power_3).toPromise())[0] || null);
    if (hero.hero_power_4)
      powers.push((await this.heroService.getPower(hero.hero_power_4).toPromise())[0] || null);

    return powers;
  }

}
