import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { FlexLayoutModule } from '@angular/flex-layout';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

//Material
import { MatInputModule } from '@angular/material/input';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatExpansionModule } from '@angular/material/expansion';



let material = [
  MatInputModule,
  MatSnackBarModule,
  MatDialogModule,
  MatIconModule,
  MatButtonModule,
  MatExpansionModule
];
let other = [
  FormsModule,
  ReactiveFormsModule,
  FlexLayoutModule,
  NgbModule
];



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ...material,
    ...other
  ],
  exports: [
    ...material,
    ...other
  ]
})
export class GlobalModule { }
