import { Component, OnInit, OnDestroy } from '@angular/core';
import { ShopService } from '../services/shop.service';
import { UserService } from '../services/user.service';
import { takeWhile } from 'rxjs/operators';
import { HeroCard, Power } from "../layout/hero-card/hero.class";

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.scss']
})
export class ShopComponent implements OnInit, OnDestroy {
  alive: boolean = true;
  heroes = [];
  user_heroes = [];
  credits = 0;

  constructor(private shopService: ShopService, private userService: UserService) {
    

    this.shopService.getHeroes().pipe(takeWhile(() => this.alive)).subscribe((heroes: Array<HeroCard>) => {

      if (heroes && heroes.length) {
        this.heroes = [...heroes];
        heroes.map(h => {
          h['collapsed'] = true;
          return h
        })
      }
        console.log(heroes);
    })

    this.userService.getUserCredits().pipe(takeWhile(() => this.alive)).subscribe((data: any) => {
      this.credits = data.user_credits;
      this.userService.userCredits = data.user_credits;
    })

  }
  ngOnDestroy(): void {
    this.alive = false;
  }

  async ngOnInit() {

    let heroes = (await this.shopService.getHeroes().toPromise()) as Array<any>;
    let user_heroes = (await this.userService.getUserHeroes().toPromise()) as Array<any>;
    if (heroes && heroes.length) {
      this.heroes = heroes.map(h => {
        h['owned'] = user_heroes.find(e => e.hero_uuid == h.hero_uuid) ? true : false;
        h['collapsed'] = true;
        return h
      });
    }

  }

}
