import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutModule } from './layout/layout.module';


const routes: Routes = [
  {path: '', loadChildren: () => {
    let token = localStorage.getItem('token');
    if (token)
      return import('./layout/layout.module').then(m => m.LayoutModule);
    else
      return import('./intro/intro.module').then(m => m.IntroModule);
  }}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
